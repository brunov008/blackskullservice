@echo OFF

:: @Parameters
:: /b : Start application without creating a new window. The
:: application has ^C handling ignored. Unless the application
:: enables ^C processing, ^Break is the only way to interrupt the application
:: -sV: Detect open ports also determine service/version informations
:: -O: Enable Operation System detection

:: Check if first parameter is empty
IF [%1]==[] GOTO nullFirstParameter

:: Check if Second parameter is empty
IF [%2]==[] GOTO nullSecondParameter
	
SET ip=%1
SET option=%2

IF port==%option% GOTO scanPorts
IF os==%option% GOTO scanOs
 
echo Comando invalido! Use como o exemplo: hack 192.167.32.1 os/port
GOTO eof

:nullFirstParameter
echo Necessario passar o ip no primeiro parametro
goto commonexit

:nullSecondParameter
echo Necessario passar uma opcao no segundo parametro
goto commonexit

:scanOs
@start /b nmap -O %ip%
goto commonexit

:scanPorts
@start /b nmap -sV %ip%
goto commonexit

:commonexit
:eof


