#!/bin/bash 

# @Parameters
# -sV: Detect open ports also determine service/version informations
# -O: Enable Operation System detection

NMAP_SCRIPT_PATH="/usr/local/bin/nmap"

ip=$1
option=$2

if [ -z $ip ]; then
	echo Necessario passar o ip no primeiro parametro
	exit
elif [ -z $option ]; then
	echo Necessario passar uma opcao no segundo parametro
	exit
else
	if [ $option = "os" ]; then
		sudo $NMAP_SCRIPT_PATH -O $ip
	elif [ $option = "port" ]; then
		sudo $NMAP_SCRIPT_PATH -sV $ip
	else
		echo Comando invalido! Use como o exemplo: ./hack.sh 192.162.32.1 os/port
	fi
fi