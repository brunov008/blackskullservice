const app = require('./app/config/express');
const mongoose = require('./app/config/mongoose');

//connect to mongoose in future
//mongoose.connect();

app.listen(app.get('port'), () => console.log(`server started on port `, app.get('port')));

module.exports = app;