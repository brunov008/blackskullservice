const app = require('express')();
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const routes = require('../api/v1/routes');
const cors = require('cors');

app.set('port', (process.env.port || 3032));

app.use(cors());
app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: false, parameterLimit: 1000000}));
app.use(bodyParser.json());
app.use(morgan('dev'));

app.use(express.static('./app/public'));

// routes
app.use('/api/v1', routes);

module.exports = app;