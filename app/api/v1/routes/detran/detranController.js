const sinesp = require('sinesp-api');

module.exports.carInfoController = async (req,res) => {
	let {placa} = req.body;

	if (!placa) return res.json({response:"Missing parameters"}) 

	let vehicle = await sinesp.search(placa)

	res.json({response:vehicle})
}