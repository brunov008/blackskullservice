const detran = require('express').Router();
const {carInfoController} = require('./detranController');

detran.post('/carInfo', carInfoController);

module.exports = detran;