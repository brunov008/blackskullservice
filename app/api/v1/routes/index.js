const router = require('express').Router();

const hack = require('./hack');
const detran = require('./detran');

router.use('/hack', hack);
router.use('/detran', detran);

module.exports = router;