const hack = require('express').Router();
const {execController} = require('./hackController');

hack.post('/exec', execController);

module.exports = hack;