const ApiError = require('../../util/ApiError');
const child_process  = require('child_process');
const {current_op} = require('../../../../config/vars');

module.exports.execController = async (req, res) => {
	try{
		let {ip, option} = req.body

		if (!ip || !option) return res.json({response:"Missing parameters"}) 

		var command;

		switch(current_op){
			case 'Mac':
				command = "./hack.sh "+ip+" "+option
				break

			case 'Windows':
				command = "hack.bat "+ip+" "+option
				break

			default:
				res.json({response:"Erro ao inicializar o script"})
		}

		child_process.exec(command,(error, stdout, stderr) => {

			console.log(stdout)
			console.log(stderr)

			if (error) return res.json({response:error})
			
		    res.json({response:stdout})
		})

	}catch(e){
		res.json({response:e})
	}
}